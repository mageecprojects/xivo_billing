<?php
ini_set("display_errors",0);
require_once('lib/PDOConfig.php');
require_once('lib/Tools.php');
//SELECT * FROM pg_proc WHERE proname = 'lastg' AND proisagg

/*CREATE OR REPLACE FUNCTION public.first_agg ( anyelement, anyelement )
RETURNS anyelement LANGUAGE sql IMMUTABLE STRICT AS $$
        SELECT $1;
$$;

		-- And then wrap an aggregate around it
CREATE AGGREGATE public.first (
        sfunc    = public.first_agg,
        basetype = anyelement,
        stype    = anyelement
);

-- Create a function that always returns the last non-NULL item
CREATE OR REPLACE FUNCTION public.last_agg ( anyelement, anyelement )
RETURNS anyelement LANGUAGE sql IMMUTABLE STRICT AS $$
        SELECT $2;
$$;

						-- And then wrap an aggregate around it
CREATE AGGREGATE public.last (
        sfunc    = public.last_agg,
        basetype = anyelement,
        stype    = anyelement
);
*/
//$db=new PDOConfig("pgsql","192.168.104.2","asterisk","xivo1","xivo1");
$db=new PDOConfig("pgsql","127.0.0.1","asterisk","xivo1","xivo1");
$final=array();
$list_user=getUserByParam();
$list_right=getRightCall("default");
//print_r($list_right);
//convertExtensionToregex("_00.");
//$list_right=getRightCall("default");
//synchroBilling();

//print_r($list_right);
//$info_context=detectAppelContext(20378523,$list_right );
//$res=getcallDurationInCel('102',null,'2013-09-25',null,null,$final);
print_r(getcallDurationInCel(null,true,'116',"2013-09-25"));
/*
foreach($res as $appel){

$test=getAppelData($appel["linkedid"] ,"210" ,$appel["firstline"] ,$appel["lastline"],null,$list_right);
print_r($test);
}
*/

//------------------------------------
function getcallDurationInCel($linkedid_list=null,$sync=false,$line=null,$date_min=null,$date_max=null,$context=null,$list_app=null){
	global $db;
	$where_clauses=array();
	$line_clauses=array();
	if($line!=null){
		$line_clauses[]=" first(cid_num)='$line'";
	}

	if($linkedid_list!=null){
		foreach($linkedid_list as $key=> $appel){
			$linkedid_list[$key]="'".$appel."'";
		}
		$linked_data=implode(",",$linkedid_list);
		$where_clauses[]="linkedid in (".$linked_data.") ";
	}

	if($date_min!=null){
			$where_clauses[]="eventtime>='$date_min'";
	}
	if($date_max!=null){
		$where_clauses[]="eventtime<='$date_max'";
	}

	if(empty($where_clauses)){
		$where_sql="";
	}else{
		 $where_sql=" where ".implode($where_clauses," and " );
	}

	if(empty($line_clauses)){
		$line_sql="";
	}else{
		$line_sql=implode($line_clauses," and " )." and ";
	}
//	$sql1="SELECT min(id) as firstline,max(id) as lastline,linkedid from public.cel {$where_sql} group by linkedid order by linkedid desc";
	$sql1="SELECT first(id) as firstline,last(id) as lastline,first(cid_num) as numappelant,first(cid_name) as nameappelant,last(eventtime) as timefin ,first(eventtime) as debappel,first(exten) as num_dest,linkedid::text,callinfo(linkedid::text,last(eventtime)::text) from public.cel
	{$where_sql} group by linkedid having {$line_sql} first(context)<>'from-extern' and last(eventtype)='LINKEDID_END' order by linkedid desc;";
$list_appel=$db->executeRequete($sql1);
	foreach($list_appel as $key=>$value){
		$infodata=appelData($value["callinfo"]);
			$infocontext=detectAppelContext($value["num_dest"]);
		$list_appel[$key]["answer"]=$infodata["time_reponse"];
		$list_appel[$key]["destinataire"]=$infodata["destinataire"];
		$list_appel[$key]["duration"]=$infodata["duration"];
		$list_appel[$key]["duration_formatted"]=$infodata["duration_formatted"];
		$list_appel[$key]["type_appel"]=$infodata["type_appel"];
		$list_appel[$key]["context"]=$infocontext["context"];
		$list_appel[$key]["context_id"]=$infocontext["id"];
		$list_appel[$key]["xivo_user_id"]=getUserIdByLine($value["numappelant"]);

		if($sync){//synchronisration demand�e

			try{
				$data_to_insert=array("id_appel"=>$value["linkedid"],
			    "id_appel"=>$value["linkedid"],
			    "type_appel"=>$infodata["type_appel"],
			    "num_appelant"=>$value["numappelant"],
			    "name_appelant"=>$value["nameappelant"],
			    "xivo_user_appelant"=>$list_appel[$key]["xivo_user_id"],
			    "num_destinataire"=>$value["num_dest"],
			    "name_destinataire"=>$infodata["destinataire"],
			    "debut_appel"=>correctTimeStamp($value["debappel"]),
			    "reponse_time"=>$infodata["time_reponse"],
			    "fin_appel"=>correctTimeStamp($value["timefin"]),
			    "duration"=>$infodata["duration"],
			    "duration_formatted"=>$infodata["duration_formatted"],
			    "context_id"=>$infocontext["id"],
			    "context_name"=>$infocontext["context"]



			);
				$db->insertData("xivo_local_billing",$data_to_insert,0,true );

			}catch (Exception $e){
				echo $e->getMessage();
			}

		}
	//	echo (($list_appel[$key]["answer"]=="NULL" )? "noreponse": "uuu").'<br>';
	}

	return $list_appel;
}
//------------------------------
function synchroBilling(){
	global $db;
	$sql_no_synch="select linkedid::text from cel where linkedid not in (select id_appel from xivo_local_billing) group by linkedid
	having first(exten)<>'from-extern' limit 2";
	$list_to_sync=$db->executeRequete($sql_no_synch);
	$array_sync=array();
	foreach($list_to_sync as $key=>$value){
		$array_sync[]=$value["linkedid"];
	}

	$info_appel=getcallDurationInCel($array_sync,true);
}


//----------------------------
function addQuote($val){

	return "'".$val."'";

}

//------------------------
function appelData($data){

	$data=str_replace("{","" ,$data);
	$data=str_replace("}","" ,$data);
	$list_info=explode(",",$data);
	$id=$list_info[0];
	$timefin=correctTimeStamp($list_info[1]);
	$timedebut=correctTimeStamp($list_info[2]);
	$denied=correctTimeStamp($list_info[4]);
//	echo $timedebut."<br>";
	if($timedebut=="NULL"){// c un appel non repondu
		$destinataire_name=="";
		$typeappell="NOANSWERED";
	//	$typeappell="NOANSWERED";
		$duration_s=0;
		$reponse="1970-01-01 00:00:00";
		$duration_formatted="00:00:00";

	}else if($denied!="NULL"){
		$destinataire_name="";
		$typeappell="DENIED";
		$duration_s=0;
		$duration_formatted="00:00:00";
		$reponse="1970-01-01 00:00:00";
	}else{//appel repondu non refuse

		$destinataire_name=trim($list_info[3]);
		$destinataire_name=str_replace("\"","" ,$list_info[3] );
		$typeappell="ANSWERED";
		$duration_s=(strtotime($timefin)-strtotime($timedebut));
		$duration_formatted=convertduration($duration_s);
		$reponse=$timedebut;
	//	print_r($diff);

	}
	return array("destinataire"=>$destinataire_name,"time_reponse"=>$reponse,"fin_appel"=>$timefin,"duration"=>$duration_s,"duration_formatted"=>$duration_formatted,"type_appel"=>$typeappell);
}
//---------------------------
function getAppelData($id_appel,$line,$key,$keymax,$apel_register=null,$list_right_appel=null){

	global $db;
	//on cherche le num de destination
		$array_result=array();
	$sql_num_dest="select exten,cid_num,cid_name,eventtime from cel where id='$key'";
	$res_num_dest=$db->executeOne($sql_num_dest);
	$num_dest=$res_num_dest["exten"];
	$name_appel=$res_num_dest["cid_name"];
	$date_appel=Tools::convert_date_to_french(correctTimeStamp($res_num_dest["eventtime"]));
	$noreponse=false;
	$time_fin=null;
	$time_deb=null;
	$dest_name=null;


	//-----------------
	// on verifie ke lappel est termin�
	$sql_fin="select eventtime  from cel where linkedid='$id_appel' and eventtype='LINKEDID_END'";
	$finappel=$db->executeOne($sql_fin);
	if(!empty($finappel)){// cet appel est termin� on peut chercher a cacller la duree
	//	echo "appelfini";
		$time_fin=$finappel["eventtime"];
	//	echo "<br>".$time_fin;
		$sql_answer="select eventtime,cid_name from cel where linkedid='$id_appel' and eventtype='ANSWER'";
		$answer_rep=$db->executeOne($sql_answer);

		if(!empty($answer_rep)){// c un appel repondu
			$time_deb=$answer_rep["eventtime"];
			$dest_name=$answer_rep["cid_name"];
		//   echo "<br>".$time_deb;
			$duration=getDBTime_with_secs(correctTimeStamp($time_fin),correctTimeStamp($time_deb) );
			$duration_formatted=convertduration($duration);
		}else{

			$noreponse=true;
			$duration=0;
			$duration_formatted="00:00:00";
		}
		//on recupere les infos de la
		//	$data_reponse=getLineAppel($answer_rep);
		//	$
	$info_context=detectAppelContext($num_dest,$list_right_appel );
	$array_result["appel"]=$id_appel;
	$array_result["line_min"]=$key;
	$array_result["line_max"]=$keymax;
	$array_result["destinataire"]=$num_dest;
	$array_result["nom_destinataire"]=$dest_name;
	$array_result["name"]=$name_appel;
	$array_result["line"]=$line;
	$array_result["date_appel"]=$date_appel;
	$array_result["duration_seconds"]=$duration;
	$array_result["duration"]=$duration_formatted;
	$array_result["context"]=$info_context["context"];
	$array_result["context_id"]=$info_context["id"];
	$array_result["noreponse"]=($noreponse)? "NON REPONDU" : "REPONDU";
//	$array_result["noreponse"]=($noreponse)? "NON REPONDU" : "REPONDU";


	}

	//on cherche le aswer
	return $array_result;

}


//-------------------------

function getLineAppel($id){
	global $db;
	$sql="select * from cel where id='$id'";
	return $db->executeOne($sql);
}
//---------------------------
function getDBTime_with_secs($a,$b){
	global $db;
	$sql_convert="SELECT EXTRACT(EPOCH FROM ('$a'::timestamp - '$b'::timestamp)) as intervall_time";
    $res=$db->executeOne($sql_convert);
	return $res["intervall_time"];

}
//-------------------------



//------------------------
function detectAppelContext($num){
    $res=array();
	$exit_loop=false;
	global $list_right;
//	var_dump($list_right);
	foreach($list_right as $key=>$droit){
		$pattern_list=explode(",",$droit["pattern"] );
	//	var_dump($pattern_list);
		foreach($pattern_list as $pattern){
		//	echo $pattern."<br>";
			if(preg_match($pattern,$num )){
				$res["id"]=$droit["id"];
				$res["context"]=$droit["name"];
				$exit_loop=true;
				break;
			}
		}

		if($exit_loop) break;
	}
	if(empty($res)){
		$res["id"]="-1";
		$res["context"]="APPEL INTERNE";
	}
//	var_dump($res);
	return $res;
}

//----------------------
function getDBTimeFormat($a,$b){
	global $db;
	$sql_convert="SELECT age ('$a'::timestamp , '$b'::timestamp)) as intervall_time";
	$res=$db->executeOne($sql_convert);
	return $res["intervall_time"];

}


//-------------------------
function correctTimeStamp($val){
	$res=explode(".",$val);
	return(str_replace("\"","",$res[0]));

}
//-------------------------

function convertduration($nbsec){
	$res="";
	$nbhours=floor($nbsec/3600);
	if($nbhours!=0){
		if($nbhours<10) $nbhours="0".$nbhours;
		$res.="{$nbhours}:";
	}else{
		$res.="00:";
	}
	$second_to_min=$nbsec%3600;

	$nbmin=floor($second_to_min/60);
	if($nbmin!=0){
		if($nbmin<10) $nbmin="0".$nbmin;
		$res.="{$nbmin}:";
	}else{
		$res.="00:";
	}

	$seconde=$second_to_min%60;
	if($seconde<10) $seconde="0".$seconde;
	$res.="{$seconde}";
	return $res;
}
//------------------------
function getRightCall($filter_param=null){


	global $db;
	if($filter_param!=NULL){
		$list_filter=explode(",",$filter_param );
		foreach($list_filter as $key=>$value){
			$list_filter[$key]="'".$value."'";
		}
			$filter=implode(",",$list_filter );
	}else{
		$filter=null;
		$where="";
	}


	if($filter!=null){
		$where="where r.name not in ($filter)";
	}
	$list_right="select r.id,r.name,string_agg(exten,',') as exten2 from rightcall as r left join rightcallexten as ext on r.id=ext.rightcallid
	{$where}
	group by r.id,r.name";
	$res=$db->executeRequete($list_right);
	foreach($res as $key=>$extension){
	//	var_dump($extension);
		$array_ext=array();
		$list_ext=explode(",",$extension["exten2"]);
		foreach($list_ext as $ext){
			$array_ext[]=convertExtensionToregex($ext);
		}
		$res[$key]["pattern"]=implode(",",$array_ext);
	//	$extension["pattern"]=convertExtensionToregex($extension["exten"]);
	}
	return $res;

}
//------------------------
function getUserByParam($key=null,$type="line",$returns=null){


	global $db;
	if($returns!=null){
		$columns=$returns;
	}else{
		$columns="u.*,ul.line_id,ul.extension_id,l.name,l.number";
	}
	$list_user="select {$columns} from userfeatures as u left join user_line as ul   on u.id=ul.user_id
	left join linefeatures as l on l.id=ul.line_id";
	if($key!=null){


	if($type=="user"){
		$list_user.=" where u.id='$key'";
	}else if($type=="line"){
		$list_user.=" where l.number='$key'";
	}

	}
	$res=$db->executeRequete($list_user,0);
	return $res;
}
//------------------
function getUserIdByLine($val){
	global $list_user;
	$res="";
	foreach($list_user as $user){
		if($user["number"]==$val){
			$res=$user["id"];
			break;
		}
	}

	return $res;
}


//-----------------
function convertExtensionToregex($ext){

	$exp=explode("_",$ext);
	$regexpattern="";

	$ext=$exp[1];
	$cptsequence=0;
	$debut_sequ=false;
	$ext=str_replace("!","[0-9]+" ,$ext );
	$ext=str_replace(".","[0-9]+" ,$ext );
//	echo $ext."<br>";
	$lg=strlen($ext);
	for($i=0;$i<$lg;$i++){

		if($ext[$i]=="X"){
		//	echo "$i ".$ext[$i]."<br>";
			if(!$debut_sequ) {
				$debut_sequ=true;
				$cptsequence=1;
			}else{
				$cptsequence++;
			}
          // echo $debut_sequ."<br>";
		}else{
			//	echo "fff"."<br>";
			if($debut_sequ){//une sequence est la
			//	echo "eeee"."<br>";
				$regexpattern.='[0-9]{'.$cptsequence.'}';
					$debut_sequ=false;
				$cptsequence=0;
			}

			 $regexpattern.=$ext[$i];
			}

		}

	if($debut_sequ){//on une sequence en cours e tla boucle est finie
		$regexpattern.='[0-9]{'.$cptsequence.'}';
		$debut_sequ=false;
		$cptsequence=0;
	}
    // echo "res".$regexpattern."<br>";
	return "/^".$regexpattern."$/i";
}
//-----------------
//print_r($res);
?>