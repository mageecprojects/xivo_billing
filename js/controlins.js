// JavaScript Document
$(document).ready(function(e) {
	
	var nom=new LiveValidation("nom_ab",{validMessage:"OK"});
	nom.add(Validate.Presence,{failureMessage:"    *ce champ est obligatoire"});
    nom.add( Validate.Format, { pattern: /^[A-Za-z.\- ]+$/i, failureMessage: " *caractères invalides" } );
	
	var numero=new LiveValidation("numero_ab",{validMessage:"OK"});
numero.add(Validate.Presence,{failureMessage:"    *ce champ est obligatoire"});
    numero.add( Validate.Format, { pattern: /^(01|02|03|40|41|42)[0-9]{6}$/i, failureMessage: " *Ce numéro n' est pas un numero moov" } );
	
	var numero2=new LiveValidation("numero2_ab",{validMessage:"OK"});
numero2.add(Validate.Presence,{failureMessage:"    *ce champ est obligatoire"});
    numero2.add( Validate.Format, { pattern: /^[0-9]{8}$/i, failureMessage: " *Ce numéro est incorrecte" } );

	var prenom=new LiveValidation("prenom_ab",{validMessage:"OK"});
	prenom.add(Validate.Presence,{failureMessage:"    *ce champ est obligatoire"});
    prenom.add( Validate.Format, { pattern: /^[A-Za-z.\- ]+$/i, failureMessage: " *caractères invalides" } );
	
	var email=new LiveValidation("email_ab",{validMessage:"OK"});
	email.add(Validate.Presence,{failureMessage:"    *ce champ est obligatoire"});
    email.add( Validate.Email, {  failureMessage: " *Email invalide" } );

    
});