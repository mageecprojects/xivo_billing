<?php

/**
 *
 *
 * @version $Id$
 * @copyright 2010
 */
class PDOConfig extends PDO {

private $engine;
private $host;
private $database;
private $user;
private $pass;



public function __construct($lengine,$lhost,$ldatabase,$luser,$lpass){
	$this->engine = $lengine;
	$this->host = $lhost;
	$this->database = $ldatabase;
	$this->user = $luser;
	$this->pass = $lpass;
	$dns = $this->engine.':dbname='.$this->database.";host=".$this->host;
	parent::__construct( $dns, $this->user, $this->pass );
	$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



}
	public function insertAll(){
		$argument=func_get_args();
		$values_arr=array();
		$from=$argument[0];
		$num_arg=func_num_args();
		$debug=$argument[$num_arg-1];
		$gensql="insert into {$from} values (";
			$values="";
		for($i=1;$i<$num_arg-1;$i++){
			if($argument[$i]==null){
					$values_arr[]="null";
			}else{
				$values_arr[]="'".$argument[$i]."'";

			}
		}

        $values=implode(",",$values_arr);
		$sql=$gensql.$values.")";
		$this->execCustom($sql,$debug,true);
	}


	public function insertData($tables,$data,$debug=0,$throw_exception=false){


		$values_arr=array();
		$columns_arr=array();
		$from=$tables;
			foreach($data as $key=>$value){
				$columns_arr[]=$key;
				if($value==null){
					$values_arr[]="null";
				}else{
					$values_arr[]="'".$value."'";
				}

			}
		$columns="";
		$values="";
		$columns=implode(",",$columns_arr );
	    $values=implode(",",$values_arr );
		$sql="insert into {$from} (".$columns.") values (".$values.")";


		$this->execCustom($sql,$debug,$throw_exception);

	}
	public function _query($sql){
		$list=$this->prepare($sql);
		$list->execute();
		$result=$list->fetchAll(PDO::FETCH_ASSOC);
		$list->closeCursor();
		return $result;
	}

	//--------------------------
	public function _queryOne($sql){

		$list=$this->prepare($sql);
		$list->execute();
		$result=$list->fetch(PDO::FETCH_ASSOC);
		$list->closeCursor();
		if(empty($result)) $result=array();
		return $result;
	}


	//--------------------------
	public function executeRequete($sql,$debug=0,$throw_exception=false){
		try{
			$list=$this->prepare($sql);
			$list->execute();
			$result=$list->fetchAll(PDO::FETCH_ASSOC);
			$list->closeCursor();
		}catch (PDOException $e){
			if(getenv("APPLICATION_ENV")!="PRODUCTION"){

				if(!$throw_exception){
					echo "reket=".$sql."<br>retour=".$e->getMessage();

				}
            }

			if ($throw_exception) throw $e;

		}

		if($debug==1){
           	if(getenv("APPLICATION_ENV")!="PRODUCTION"){
			echo "reket=".$sql."<br>retour=".print_r($result,true);
           	}
		}

		return $result;
	}

	public function execCustom($sql,$debug=0,$throw_exception=false){
		$call_exception=false;
		try{
		$result=$this->exec($sql);
		}catch(PDOException $e){

			if(getenv("APPLICATION_ENV")!="PRODUCTION"){

				if(!$throw_exception){
					 echo "reket=".$sql."<br>retour=".$e->getMessage();
				}
			}

			if($throw_exception) throw $e;
		}
		if($debug==1){
      	if(getenv("APPLICATION_ENV")!="PRODUCTION"){
			echo "reket=".$sql."<br>retour=".print_r($result,true);
          	}
		}

	}
	public function executeOne($sql,$debug=0,$throw_exception=false){
     $error="";
		try{
			$list=$this->prepare($sql);
			$list->execute();
			$result=$list->fetch(PDO::FETCH_ASSOC);
			$list->closeCursor();
			if(empty($result)) $result=array();
		}catch(PDOException $e){
          	if(getenv("APPLICATION_ENV")!="PRODUCTION"){

          		if(!$throw_exception){
          			echo "reket=".$sql."<br>retour=".$e->getMessage();

          		}

          	if($throw_exception) throw $e;
         	}

		}
			if($debug==1){
              	if(getenv("APPLICATION_ENV")!="PRODUCTION"){
				echo "reket=".$sql."<br>retour=".print_r($result,true);
              	}
			}


		return $result;
	}

}
?>