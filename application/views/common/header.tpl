<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="{DOMAIN}">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<title>Xivo Billing</title>
<link href="css/general.css" rel="stylesheet" type="text/css" />
<link href="css/smoothness/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='js/jquery.idTabs.min.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.10.3.custom.min.js'></script>
<!--[if IE]>
<style type="text/css">
clearli {float:left;}
</style>
<![endif]-->
<script>
$(document).ready(function(e) {
	var current_val="";
	var current_selection="";
	var disable_user="{DISABLEUSER}";
	var olddroit="{OLDRIGHT}";
	var olduser="{OLDUSER}";
	var oldusername="{OLDUSERNAME}";
	var modeform="{MODEFORM}";
	var oldoperation="{filter_operation}";
//	var current_selection="";
	var liste_user=new Array();

	function isInList(_id){
		var lg=liste_user.length;
		var result=false;
		for(var i=0;i<lg;i++){

			if(liste_user[i]==_id){
				result=true;
				break;
			}
		}

		return result;
	}
	//------------------------------------
	function removeInList(_id){
		var lg=liste_user.length;
		var result=null;
		for(var i=0;i<lg;i++){

			if(liste_user[i]==_id){
				result=i;
				break;
			}
		}
		liste_user.splice(result,1);
		$('input[name="users_real"]').val(liste_user.join(","));
		return result;
	}
	//----------------------------------------------------
	function addUser(_id,_text){
		var html='<div class="usersmallbox" idu="'+_id+'"><a href="#">X</a>'+_text+'</div>';

		if(!isInList(_id)){
			liste_user.push(_id);
			$(html).appendTo(".user_box");

			$('input[name="users_real"]').val(liste_user.join(","));
			//alert($('input[name="users_real"]').val());
		}


	}
	//--------------------------------------------------------
	$("#operation").val(oldoperation);
		$(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
	//	$(".datepicker").datepicker();
	//------------------------------------------------
	if(modeform=="edit"){//le formulaire est en edit
	  //   alert("ok");
    	$('select[name="liste_users"]').attr("disabled",true);
    	$('select[name="liste_users"]').val(olduser);
			$('input[name="users_real"]').val(olduser);
		var oldhtml='<div class="usersmallboxedit" >'+oldusername+'</div>';
			$(oldhtml).appendTo(".user_box");
          $('select[name="liste_right"]').val(olddroit);
	//
	}


	//------------------------------------------------------
	$('select[name="liste_users"]').change(function(e) {
		//alert("iiii");
		current_selection=$('select[name="liste_users"] option:selected').text();
		current_val=$(this).val();
		if(current_val!='-1'){
		addUser(current_val,current_selection);
		}


	});
	//----------------------------------------------------
	$(".usersmallbox").live("click",function(e) {
		$(this).fadeOut('fast',function(e){
			removeInList($(this).attr('idu'));
			$(this).remove();


		});

	});

	//-------------------------------------------
	$(".delquota").click(function(e){

		e.preventDefault();
		var key=$(this).attr("idu");
		if(confirm("VOULEZ VRAIMENT SUPPRIMER LE QUOTA ATTRIBUE A "+$(this).attr("iduname")+" ?")){
		  window.location.href="deletequota/?key="+key;
		}

	});
	//----------------------------------------

	$("#btnvalidquota").click(function(e) {
       // e.preventDefault();
	//	alert("uuuu");
		if(modeform!="edit"){

		//alert("iiiiii");
				var user_selected_list=$('input[name="users_real"]').val();
			var numexp=/^[123456789][0-9]*$/gi;
		var quota=$('input[name="quota"]').val()
		var user_already=null;

		if(user_selected_list==""){
		alert("VEUILLEZ SELECTIONNER UN UTILISATEUR");
		}else if(!quota.match(numexp)){
		 alert("VEUILLEZ SAISIR UN QUOTA D' APPEL VALIDE");
	   }else{

        	$("#addquotaform").submit();
	//	alert(user_selected_list);
	/*	$.post("index.php?ctrl=quota&action=checkquotauser",
		{user:user_selected_list,droit:$('select[name="liste_right"]').val()},
		function(data){
     //  alert(data);
			if(data.indexOf('NOUSERINDATABASEVEONE')!=-1){
			$("#addquotaform").submit();
			}else{
			//	alert(data);
				user_already=data.split("-");
					$("#msgbox").removeAttr("class");
					$("#msgbox").text("");
				$("#msgbox").addClass("error");
				for(var i=0;i<user_already.length;i++){
			     $("#msgbox").append("l' utilisateur "+user_already[i]+" a deja un quota attribué pour ce droit d' appel<br>");
				}
				//$("#msgbox").addClass("error");
			}

		});*/
	}
		}else{//fin du mode edit du formulaire
		  	$("#addquotaform").submit();
		}
		//-----------------------------------
    });

	//------------------------------------------

});
</script>
</head>

<body>
<base href="{DOMAIN}">
	<div class="header">
		<div class="header_logo"></div>
		<div class="header_usermenu">
			Hello, <a href="#">Administrateur</a> | <a href="deconnexion/">Deconnexion</a>

		</div>
	</div>
	<div class="horizontal_menu">
		<div class="horizontal_menu_left">
			<ul>
				<li><a href="ajouterquota/">Ajouter ou retirer  du credit d' appel</a></li>
				<li><a href="quotausers/">Gestion des quota attribués</a></li>
				<li><a href="histquota/">Historique des quota attribués</a></li>
				<li><a href="stats/">Statistiques</a></li>

			</ul>
		</div>
		<!--<div class="horizontal_menu_right">
			<input type="text" class="searchBox">
		</div>-->
	</div>