<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="{DOMAIN}" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Veone Billing Local</title>
<link href="css/general.css" rel="stylesheet" type="text/css" />
</head>

<body class="login_body">
<base href="{DOMAIN}">
	<div class="login_container">
		<div class="login_logo"></div>
		<div class="spacer_20"></div>
		<div class="login_text">Administration des droits d' appel</div>
		<div class="{MESSAGE_TYPE}">{MESSAGE_CONTENT}</div>
		<div class="spacer_30"></div>
		<div class="login_form">
		<form name="form1" id="form1" action="contrologin/" method="POST">
			<div class="login_field">Login : </div>
			<div class="login_input"><input type="text" name="login"/></div>
			<div class="spacer_10"></div>
			<div class="login_field">Mot de passe : </div>
			<div class="login_input"><input type="password" name="password"/></div>
			<div class="spacer_10"></div>
			<!--<div class="login_rememberme"><input name="" type="checkbox" value="" />Remember me<br><a href="#">Forgot password?</a></div>-->
			<div class="login_submit"><input name="" type="submit" value="Login"/></div>
		</form>
		</div>
	</div>
</body>
</html>
