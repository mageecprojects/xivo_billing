<?php
require_once("baseController.php");

class quotaController extends baseController{

   public function index(){


   	$connected=$this->status_connexion();
   	if($connected){
   		try{


   		$this->vue->set_filenames(array("header"=>"common/header.tpl","menu"=>"common/menu.tpl","body"=>"operations/ListQuota.tpl"));


   	//	$pag
   		//-----------------------



   		$page=Tools::getQueryValue("page");
   		$where_clauses=array();
   		$nom=Tools::getQueryValue("nom");
   		$droit_filtre=Tools::getQueryValue("droit");
   		$quota_filtre=Tools::getQueryValue("quota");

   		if($nom!=""){
   			$where_clauses[]=" callerid ~* '$nom'";
   		}

   		if($droit_filtre!=""){
   				$where_clauses[]=" r.name ~* '$droit_filtre'";
   		}

   		if($quota_filtre!=""){
   			$where_clauses[]=" q.quota_text ~* '$quota_filtre'";
   		}

   		if(!empty($where_clauses)){

   			$where=" where ".implode(" and ",$where_clauses)." ";
   		}
   		if($page=="") $page=1;
   		$paging=10;
   		$borneinf=($page-1)*$paging;

   		$quotaattribuate="select q.id,u.callerid,r.name,q.quota,q.quota_text,q.date_quota from quota_billing as q inner join userfeatures as u
   		on q.user_id=u.id
   		inner join rightcall as r on q.right_id=r.id
   		{$where}
		 order by q.id desc LIMIT $paging OFFSET $borneinf ";


   		$list_quota=$this->database->executeRequete($quotaattribuate,0,true);
   		$total_quota="select count(*) as number from quota_billing as q inner join userfeatures as u
   		on q.user_id=u.id
   		inner join rightcall as r on q.right_id=r.id
   		{$where}";
   		$total_res=$this->database->executeOne($total_quota,0,true);

   		$last_page=ceil(intval($total_res["number"])/$paging);
   	//	echo $last_page;
   	//	$last_page=20;
   		$pagination=$this->generatePagination($last_page,15 ,"quotausers/" ,"page");


   		foreach($list_quota as $quota){
   			$this->vue->assign_block_vars("QUOTA",array(
   		"owner"=>$this->removeQuote($quota["callerid"]),
   		"droit"=>$this->removeQuote($quota["name"]),
   		"amount"=>$this->removeQuote($quota["quota_text"]),
   		"id"=>$quota["id"],
   		"date"=>Tools::convert_date_to_french($quota["date_quota"]),
		) );
   		}

   		if(empty($list_quota)){
   			$this->addMessage($this->MES_ERROR,"Aucun resultat trouvé dans la base de données" );
   		}
   		$this->vue->assign_var("pagination",$pagination );
   		$this->vue->assign_var("filter_nom",htmlspecialchars($nom) );
   		$this->vue->assign_var("filter_droit",htmlspecialchars($droit_filtre) );
   		$this->vue->assign_var("filter_quota",htmlspecialchars($quota_filtre) );
   		$this->vue->assign_var("MESSAGE_TYPE",$this->getMessagetype() );
   		$this->vue->assign_var("MESSAGE_CONTENT",$this->getMessageContent() );

   		$this->vue->assign_var("pagination",$pagination );
   		$this->vue->assign_var_from_handle("HEADER",'header');
   		$this->vue->assign_var_from_handle("MENU",'menu');
   		$this->vue->pparse('body');
   		}catch(Exception $e){

   			$this->showErrorPage($e->getMessage());
   		}
   	}else{
   		Tools::redirect($this->domain.$this->login_url);
   	}

   }

	//-------------------------------------
	public function indexhistory(){


		$connected=$this->status_connexion();
		if($connected){
			try{


			$this->vue->set_filenames(array("header"=>"common/header.tpl","menu"=>"common/menu.tpl","body"=>"operations/ListHistory.tpl"));


			//	$pag
			//-----------------------



			$page=Tools::getQueryValue("page");
			$where_clauses=array();
			$nom=Tools::getQueryValue("nom");
			$droit_filtre=Tools::getQueryValue("droit");
			$quota_filtre=Tools::getQueryValue("quota");
			$quota_operation=Tools::getQueryValue("operation");
			$date_filtre=Tools::getQueryValue("dateappel");

			if($nom!=""){
				$where_clauses[]=" nom_utilisateur ~* '$nom'";
			}

			if($droit_filtre!=""){
				$where_clauses[]=" droit_appel ~* '$droit_filtre'";
			}

			if($quota_filtre!=""){
				$where_clauses[]=" quota_text ~* '$quota_filtre'";
			}

			if($quota_operation!=-1){
					$where_clauses[]=" operation_quota ~* '$quota_operation'";
			}

			if($date_filtre!=""){
				$where_clauses[]=" date_quota::text ~* '$date_filtre'";
			}

			if(!empty($where_clauses)){

				$where=" where ".implode(" and ",$where_clauses)." ";
			}
			if($page=="") $page=1;
			$paging=10;
			$borneinf=($page-1)*$paging;

			$quotaattribuate="select * from quota_billing_history
   		{$where}
		 order by id desc LIMIT $paging OFFSET $borneinf ";


			$list_quota=$this->database->executeRequete($quotaattribuate,0,true);
			$total_quota="select count(*) as number from quota_billing_history
   		{$where}";
			$total_res=$this->database->executeOne($total_quota,0,true);

			$last_page=ceil(intval($total_res["number"])/$paging);
			//	echo $last_page;
			//	$last_page=20;
			$pagination=$this->generatePagination($last_page,15 ,"histquota/" ,"page");


			foreach($list_quota as $quota){
				$this->vue->assign_block_vars("QUOTA",array(
				"owner"=>$this->removeQuote($quota["nom_utilisateur"]),
				"droit"=>$this->removeQuote($quota["droit_appel"]),
				"amount"=>$this->removeQuote($quota["quota_text"]),
				"operation"=>$quota["operation_quota"],
				"date"=>Tools::convert_date_to_french($quota["date_quota"]),
			) );
			}

			if(empty($list_quota)){
				$this->addMessage($this->MES_ERROR,"Aucun resultat trouvé dans la base de données" );
			}
			$this->vue->assign_var("pagination",$pagination );
			$this->vue->assign_var("filter_nom",htmlspecialchars($nom) );
			$this->vue->assign_var("filter_operation",$quota_operation );
			$this->vue->assign_var("filter_droit",htmlspecialchars($droit_filtre) );
			$this->vue->assign_var("filter_quota",htmlspecialchars($quota_filtre) );
			$this->vue->assign_var("filter_date",htmlspecialchars($date_filtre) );
			$this->vue->assign_var("pagination",$pagination );
			$this->vue->assign_var_from_handle("HEADER",'header');
			$this->vue->assign_var_from_handle("MENU",'menu');
			$this->vue->assign_var("MESSAGE_TYPE",$this->getMessagetype() );
			$this->vue->assign_var("MESSAGE_CONTENT",$this->getMessageContent() );

			$this->vue->pparse('body');
			}catch(Exception $e){
				$this->showErrorPage($e->getMessage());
			}
		}else{
			Tools::redirect($this->domain.$this->login_url);
		}

	}



	//-------------------------------------
	public function stats(){


		$connected=$this->status_connexion();
		if($connected){
			try{


			$this->vue->set_filenames(array("header"=>"common/header.tpl","menu"=>"common/menu.tpl","body"=>"operations/stats.tpl"));


			//	$pag
			//-----------------------



			$page=Tools::getQueryValue("page");
			$where_clauses=array();
			$nom=Tools::getQueryValue("nom");
			$droit_filtre=Tools::getQueryValue("droit");
			$quota_filtre=Tools::getQueryValue("duration");
			$num_filtre=Tools::getQueryValue("numero");
		//	$duration_filtre=Tools::getQueryValue("duration");
			$date_filtre=Tools::getQueryValue("debappel");
			$date_filtre_fin=Tools::getQueryValue("finappel");
		/*	if($date_filtre!=""){
				list($day,$month,$year)=explode("/",$date_filtre);
				$date_filtre=$year."-".$month."-".$day;
			}*/

			$quota_operation=Tools::getQueryValue("operation");
			//$quota_filtre=Tools::getQueryValue("quota");

			if($nom!=""){
				$where_clauses[]=" u.callerid ~* '$nom'";
			}

			if($droit_filtre!=""){
				$where_clauses[]=" r.name ~* '$droit_filtre'";
			}

			if($num_filtre!=""){
				$where_clauses[]=" b.num_destinataire like '$num_filtre%'";
			}

			if($date_filtre!=""){

				$where_clauses[]=" b.debut_appel::timestamp >= '$date_filtre'";
			}

				if($date_filtre_fin!=""){
						$fin_filtre=$date_filtre_fin." 23:59:59";
					$where_clauses[]=" b.debut_appel::timestamp <= '$fin_filtre'";
				}

			if($quota_filtre!=""){
				$where_clauses[]=" b.duration_formatted ~* '$quota_filtre'";
			}

			if(($quota_operation!=-1)&&($quota_operation!="")){
				$where_clauses[]=" b.type_appel = '$quota_operation'";
			}

			if(!empty($where_clauses)){

				$where=" where ".implode(" and ",$where_clauses)." ";
			}
			if($page=="") $page=1;
			$paging=10;
			$borneinf=($page-1)*$paging;

			$quotaattribuate="select callerid,r.name as droit_appel,debut_appel,type_appel,duration_formatted,num_destinataire from xivo_local_billing as b inner JOIN
			userfeatures as u on b.xivo_user_appelant=u.id
			inner join rightcall as r on b.context_id=r.id
   		{$where}
		 order by b.id desc LIMIT $paging OFFSET $borneinf ";


			$list_quota=$this->database->executeRequete($quotaattribuate,0,true);
			$total_quota="select count(*) as number from xivo_local_billing as b inner JOIN
			userfeatures as u on b.xivo_user_appelant=u.id
			inner join rightcall as r on b.context_id=r.id
   		{$where}";
			$total_res=$this->database->executeOne($total_quota);

			$last_page=ceil(intval($total_res["number"])/$paging);
			//	echo $last_page;
			//	$last_page=20;
			$pagination=$this->generatePagination($last_page,15 ,"stats/" ,"page");


			foreach($list_quota as $quota){
				$this->vue->assign_block_vars("QUOTA",array(
				"owner"=>$this->removeQuote($quota["callerid"]),
				"droit"=>$this->removeQuote($quota["droit_appel"]),
				"dest"=>$this->removeQuote($quota["num_destinataire"]),
				"duration"=>$quota["duration_formatted"],
				"date"=>Tools::convert_date_to_french($quota["debut_appel"]),
				"status"=>($quota["type_appel"]=="ANSWERED") ? "REPONDU" :"NON REPONDU"
			//	"status"=>$quota["type_appel"]
			) );
			}

			if(empty($list_quota)){
				$this->addMessage($this->MES_ERROR,"Aucun resultat trouvé dans la base de données" );
			}
			$this->vue->assign_var("pagination",$pagination );
			$this->vue->assign_var("filter_nom",htmlspecialchars($nom) );
			$this->vue->assign_var("filter_operation",$quota_operation );
			$this->vue->assign_var("filter_droit",htmlspecialchars($droit_filtre) );
			$this->vue->assign_var("filter_num",htmlspecialchars($num_filtre) );
			$this->vue->assign_var("filter_date",htmlspecialchars($date_filtre) );
			$this->vue->assign_var("filter_date_fin",htmlspecialchars($date_filtre_fin) );
			$this->vue->assign_var("filter_duration",htmlspecialchars($quota_filtre) );
			$this->vue->assign_var("pagination",$pagination );
			$this->vue->assign_var_from_handle("HEADER",'header');
			$this->vue->assign_var_from_handle("MENU",'menu');
			$this->vue->assign_var("MESSAGE_TYPE",$this->getMessagetype() );
			$this->vue->assign_var("MESSAGE_CONTENT",$this->getMessageContent() );

			$this->vue->pparse('body');
			}catch(Exception $e){
				$this->showErrorPage($e->getMessage());
			}
		}else{
			Tools::redirect($this->domain.$this->login_url);
		}

	}
    //-----------------------------------------
	public function formquota(){


		$connected=$this->status_connexion();
		if($connected){

			$this->vue->set_filenames(array("header"=>"common/header.tpl","menu"=>"common/menu.tpl","body"=>"operations/addQuota.tpl"));

			foreach($this->list_user as $user){
				$this->vue->assign_block_vars("LISTEUSER",array("id"=>$user["id"],"nom"=>$this->removeQuote($user["callerid"])) );
			}

			foreach($this->list_right as $right){
				$this->vue->assign_block_vars("RIGHTCALL",array("id"=>$right["id"],"nom"=>$this->removeQuote($right["name"])) );
			}
			//	$pag
			//-----------------------
			$mode=Tools::getQueryValue("mode");

			if($mode=="edit"){//formulaire en mode edition
				$id_quota=Tools::getQueryValue("key");
				$quotainfo="select q.id,u.callerid,r.name,r.id as rightid,q.quota,q.date_quota,u.id as userid from quota_billing as q inner join userfeatures as u
   		on q.user_id=u.id
   		inner join rightcall as r on q.right_id=r.id where q.id='$id_quota' ";

				$info_quota_res=$this->database->executeOne($quotainfo);
				//------------on affiche automatiquement le formulaire ds les ongles

				$this->vue->assign_var("SHOWFORM","selected" );
				$this->vue->assign_var("DISABLEUSER","true" );
				$this->vue->assign_var("OLDUSER",$info_quota_res["userid"] );
				$this->vue->assign_var("OLDRIGHT",$info_quota_res["rightid"] );
				$this->vue->assign_var("OLDUSERNAME",$this->removeQuote($info_quota_res["callerid"] ));
				$this->vue->assign_var("OLDQUOTA",$info_quota_res["quota"] );
				$this->vue->assign_var("MODEFORM","edit");
				$this->vue->assign_var("CURRENTUPDATELINE",$id_quota);


				//	$list_quota=$this->database->executeRequete($quotaattribuate);

			}

			//	echo $last_page;
			//	$last_page=20;

		//	$this->vue->assign_var("pagination",$pagination );
			$this->vue->assign_var_from_handle("HEADER",'header');
			$this->vue->assign_var_from_handle("MENU",'menu');
			$this->vue->pparse('body');

		}else{
			Tools::redirect($this->domain.$this->login_url);
		}
	}
	public function checkquotauser(){

		$user_to_insert=$_POST["user"];
		$droit=$_POST["droit"];
		$sqlcheck="select callerid from quota_billing as q left join userfeatures as u on u.id=q.user_id where q.user_id in ({$user_to_insert}) and right_id='$droit'";
		$res=$this->database->executeRequete($sqlcheck);

		if(empty($res)){
			echo "NOUSERINDATABASEVEONE";
		}else{
			$final=array();
			foreach($res as $user){
				$final[]=$this->removeQuote($user["callerid"]);
			}

			echo implode("-",$final );
		}


	}
	//-------------------------------
	public function deletequota(){

		$key=$_GET["key"];

		$sql="delete from quota_billing where id='$key'";
		$this->database->exec($sql);
		$this->addMessage($this->MES_SUCESS,"La supression de ce quota s' est déroulé avec succès" );
		Tools::redirect($this->domain.$this->quota_url);

	}
	//----------------------------
	public function addquotatouser(){

		$connected=$this->status_connexion();
        // $liste_users_xivo=getUserByParam();
		if($connected){
			try{


			$droit_appel=$_POST["liste_right"];
			$user=$_POST["users_real"];
			$quota=$_POST["quota"];
			$time=date('Y-m-d H:i:s');
			$quota=intval($quota*60);
			$update=$_POST["users_update"];
			$operation=$_POST["liste_operation"];
			$operation_nom=($operation==1)? "AJOUT DE CREDIT " : "RETRAIT DE CREDIT";
			$credit=($operation==1)? $quota : ($quota*(-1));
			$credit=intval($credit);
			$this->database->beginTransaction();
			if($update==""){//on est en mode insertion
				$liste_users=explode(',',$user );

				foreach($liste_users as $user){

					$infos_user=$this->getUserInfosById($user);
					$username=$this->removeQuote($infos_user["callerid"]);
					$data=array("user_id"=>$user,"right_id"=>$droit_appel,"quota"=>$quota,"date_quota"=>$time,"quota_text"=>$this->convertduration($quota));
					$data_history=array("user_id"=>$user,"quota_text"=>$this->convertduration($quota),"right_id"=>$droit_appel,"nom_utilisateur"=>$username,"droit_appel"=>$this->getRightName($droit_appel),"operation_quota"=>$operation_nom,"quota"=>$quota,"date_quota"=>$time);
					$this->database->insertData("quota_billing_history",$data_history,0 );

					//il a une ligne de quota
					$this->createQuota($droit_appel,$user );

					//
					$ligne_quota=$this->database->executeOne("select * from quota_billing where user_id='$user' and right_id='$droit_appel' for update",0,true);
					$old_quota=$ligne_quota["quota"];
					$new_quota=intval($old_quota+$credit);

					if($new_quota<=0) {
						$new_quota='0';
						$new_quota_text="00:00:00";
					}
						$new_quota_text=$this->convertduration($new_quota);
					$this->database->exec("update quota_billing set quota='$new_quota',quota_text='$new_quota_text' where right_id='$droit_appel' and user_id='$user'");
					//$this->database->insertData("quota_billing",$data,0 );
					$sql_check_xivo="select count(*) as number from rightcallmember where typeval='$user' and rightcallid='$droit_appel' and type='user'";
					$has_right=$this->database->executeOne($sql_check_xivo);

					if($has_right["number"]==0){//l utilisateur na pas ce droit attribué ds le xivo
						if($new_quota>0){
							$data2=array("rightcallid"=>$droit_appel,"type"=>'user',"typeval"=>$user);
							$this->database->insertData("rightcallmember",$data2 );
						}

					}
					$this->addMessage($this->MES_SUCESS,"l' opération d' ajout des quota d' appel s' est déroulé avec succès " );

				}
			}else{//c un update
				$info_update="select * from quota_billing where id='$update'";
				$res_info_update=$this->database->executeOne($info_update);
				$duplicate_right=$this->database->executeOne("select id from quota_billing where right_id='$droit_appel' and user_id='$user'");
				$duplicate_right=$duplicate_right["id"];

			//	echo $duplicate_right."<br>";
			//	echo $update."<br>";

				if(($duplicate_right!=$update)&&($duplicate_right!="")){
					$this->addMessage($this->MES_ERROR,"Ce droit d' appel a dejà un quota attribué pour cet utilisateur" );
				}else{

					$previous_right=$res_info_update["right_id"];
					if($previous_right!=$droit_appel){ //le droit a été modifié on droit supprimer l
						$this->database->exec("delete from rightcallmember where type='user' and rightcallid='$previous_right' and typeval='$user'");
						$this->database->exec("delete from rightcallmember where type='user' and rightcallid='$droit_appel' and typeval='$user'");
						$data3=array("rightcallid"=>$droit_appel,"type"=>'user',"typeval"=>$user);
						$this->database->insertData("rightcallmember",$data3 );
					}
					$sql_update="update quota_billing set right_id='$droit_appel',quota='$quota' where id='$update'";
					$this->database->exec($sql_update);
					$this->addMessage($this->MES_SUCESS,"la modification des quota aux utilisateurs s' est déroulé avec succès" );


				}

			}
                $this->database->commit();
				Tools::redirect($this->domain.$this->quota_url);

		//	echo "jjjjjjjjjjjjjj";
			}catch(Exception $e){
				$this->database->rollBack();

				$this->showErrorPage($e->getMessage());
			}
		}else{
			Tools::redirect($this->domain.$this->login_url);
		}
	}


}

?>