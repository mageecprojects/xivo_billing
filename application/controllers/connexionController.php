<?php
require_once("baseController.php");

class connexionController extends baseController{

   public function index(){


   	$connected=$this->status_connexion();
   	if($connected){

   		Tools::redirect($this->domain.$this->quota_url);
   	}else{
   		$this->vue->set_filenames(array("body"=>"connexion/connexion.tpl"));
   		$this->vue->pparse("body");
   	}

   }



	public function error(){
	//	print_r($_SESSION);
		$this->vue->set_filenames(array("header"=>"common/header.tpl","menu"=>"common/menu.tpl","body"=>"operations/error.tpl"));
		$this->vue->assign_var_from_handle("HEADER",'header');
		$this->vue->assign_var_from_handle("MENU",'menu');
	/*	$this->vue->assign_var("MESSAGE_TYPE",$this->getMessagetype() );
		$this->vue->assign_var("MESSAGE_CONTENT",$this->getMessageContent() )*/;

		$this->vue->pparse('body');
	//	$this->vue->pparse("body");
	}
	public function deconnexion(){
		unset($_SESSION["islogged"]);
		session_destroy();
		Tools::redirect($this->domain.$this->login_url);
	}
	public function connect(){

		$login=$_POST["login"];
		$password=$_POST["password"];
	//	print_r($_POST);
		if(($login==$this->user_login)&&($password==$this->user_password)){
			$_SESSION["islogged"]="OK";
			Tools::redirect($this->domain.$this->quota_url);
		//	echo "iiiii";
		}else{
			$this->addMessage($this->MES_ERROR,"Login ou mot de passe incorrecte" );
			Tools::redirect($this->domain.$this->login_url);
		}



	}


}

?>