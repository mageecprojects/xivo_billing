<?php
/**
 * @todo -c Implement . remettre le https sur le domaine avant d emettre en production
 */
class baseController{
	/**
	 * Constuctor
	 * priv
	 */
	protected $database;
	protected $vue;
	protected $current_user;
	protected $generaltitle;
	protected $abregedtitle;
	protected $logstatus;
	protected $domain;
	protected $MES_SUCESS;
	protected $MES_ERROR;
	protected $MES_WARNING;
	protected $MES_INFOS;
	protected $previous_url;
	protected $login_url;
	protected $inscription_url;
	protected $logout_url;
	protected $forget_url;
	protected $offres_url;
	protected $dashbord_url;
	protected $quota_url;
	protected $user_login;
	protected $user_password;
	protected $list_right;
	protected $list_user;
	protected $context_enter;
	protected $debut_synchro;


//	protected $contact_clientele_moov;





	function __construct(){

        //	session_start();
	//	print_r($_SESSION);
	$this->domain="http://".$_SERVER["HTTP_HOST"].dirname($_SERVER['PHP_SELF'])."/";
		//$this->domain="https://".$_SERVER["HTTP_HOST"].dirname($_SERVER['PHP_SELF'])."/";


     //  require_once('config.php');

		$this->MES_SUCESS="success";
		$this->MES_ERROR="error";
		$this->MES_WARNING="warning";
		$this->MES_INFOS="info";
		$this->login_url="connexion/";
		$this->dashbord_url="dashboard/";
		$this->quota_url="quotausers/";
	//	$this->login_url="?ctrl=inscription&action=connexion";
	//	$this->inscription_url="?ctrl=inscription&action=inscription";
	   require_once('config.php');
$this->context_enter=$context_entrant;
		$this->debut_synchro=$date_debut_synchro;

	$this->database=new PDOConfig('pgsql',$DB_HOST,$DB_DATABASE,$DB_USER,$DB_PASS);
		$this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	//	mysql_connect($DB_HOST,$DB_USER,$DB_PASS);

			$this->vue=new Template('application/views/');

           $this->generaltitle="XIVO BILLING";
		//	$this->vue=new Template('application/views/');
		$this->vue->assign_var("PAGE_TITLE",$this->generaltitle );
		$_SESSION["previous"]=$this->dashbord_url;
		$this->user_login="moovuser";
		$this->user_password="moovpwd";
		$this->assignConnInfos();

        $this->configuredb();
		$this->list_right=$this->getRightCall($filter);
          $this->list_user=$this->getUserByParam();

	}



	//--------------------------------------
	public function configuredb(){


		try{


		$sql_check_last="SELECT * FROM pg_proc WHERE proname = 'last' AND proisagg";
		$check_last=$this->database->executeRequete($sql_check_last,0);

		if(empty($check_last)){
			$this->database->exec("CREATE OR REPLACE FUNCTION public.last_agg ( anyelement, anyelement )
		   RETURNS anyelement LANGUAGE sql IMMUTABLE STRICT AS \$\$
		   SELECT \$2;
		   \$\$;");

		$this->database->exec("CREATE AGGREGATE public.last (
		   sfunc    = public.last_agg,
		   basetype = anyelement,
		   stype    = anyelement
		   )");

		}

		$sql_check_first="SELECT * FROM pg_proc WHERE proname = 'first' AND proisagg";
		$check_first=$this->database->executeRequete($sql_check_first);

		if(empty($check_first)){
			$this->database->exec("CREATE OR REPLACE FUNCTION public.first_agg ( anyelement, anyelement )
		   RETURNS anyelement LANGUAGE sql IMMUTABLE STRICT AS \$\$
		   SELECT $1;
		   \$\$");

			$this->database->exec("CREATE AGGREGATE public.first (
		   sfunc    = public.first_agg,
		   basetype = anyelement,
		   stype    = anyelement
		   )");



		}

			$this->database->exec("

 CREATE OR REPLACE FUNCTION callinfo(anyelement, anyelement)
  RETURNS anyarray AS
\$BODY\$

SELECT ARRAY[\$1,\$2,(SELECT first(eventtime::text) from cel where linkedid=\$1 and eventtype='ANSWER' and appname='AppDial'),(SELECT first(cid_name) from cel where linkedid=\$1 and eventtype='ANSWER' and appname='AppDial'),(SELECT first(eventtime::text) from cel where linkedid=\$1 and exten='deny')];
\$BODY\$
  LANGUAGE sql VOLATILE


         ");


			$check_quota_exists="select count(tablename) as number from pg_tables where tablename='quota_billing'";
			$res_table_quota=$this->database->executeOne($check_quota_exists);

			$check_billing_exists="select count(tablename) as number from pg_tables where tablename='xivo_local_billing'";
			$res_table_billing=$this->database->executeOne($check_billing_exists);

			$check_billing_history_exists="select count(tablename) as number from pg_tables where tablename='quota_billing_history'";
			$res_table_billing_hist=$this->database->executeOne($check_billing_history_exists);
			if($res_table_quota["number"]==0){
				$this->database->exec("CREATE TABLE quota_billing
(
  id serial NOT NULL,
  user_id integer,
  right_id integer,
  quota integer,
  date_quota timestamp without time zone,
  quota_text text,
  CONSTRAINT quota_billing_pkey PRIMARY KEY (id)
)
			");
			}

            if($res_table_billing["number"]==0){
            	$this->database->exec("CREATE TABLE xivo_local_billing(
  id serial NOT NULL,
  id_appel text,
  type_appel text,
  num_appelant text,
  name_appelant text,
  xivo_user_appelant integer,
  num_destinataire text,
  name_destinataire text,
  debut_appel timestamp without time zone,
  reponse_time timestamp without time zone,
  fin_appel timestamp without time zone,
  duration integer,
  duration_formatted text,
  context_id integer,
  context_name text,
  bill text,
  CONSTRAINT primar_key PRIMARY KEY (id),
  CONSTRAINT xivo_local_billing_id_appel_key UNIQUE (id_appel)
)");

            }


	 if($res_table_billing_hist["number"]==0){

	 	$this->database->exec(
	 		"CREATE TABLE quota_billing_history
	 		(
	 		id serial NOT NULL,
	 		user_id integer,
	 		right_id integer,
	 		quota integer,
	 		operation_quota text,
	 		date_quota timestamp without time zone,
	 		droit_appel text,
	 		nom_utilisateur text,
	 		quota_text text,
	 		CONSTRAINT quota_billing_history_pkey PRIMARY KEY (id)
	 		)");

	 }
		}catch(Exception $e){
		 //  $this->addMessage($this->MES_ERROR,$e->getMessage() );
		//	$this->showErrorPage();
			echo $e->getMessage();
		}
		//SELECT * FROM pg_proc WHERE proname = 'lastg' AND proisagg

		/*CREATE OR REPLACE FUNCTION public.first_agg ( anyelement, anyelement )
		   RETURNS anyelement LANGUAGE sql IMMUTABLE STRICT AS $$
		   SELECT $1;
		   $$;

		   -- And then wrap an aggregate around it
		   CREATE AGGREGATE public.first (
		   sfunc    = public.first_agg,
		   basetype = anyelement,
		   stype    = anyelement
		   );

		   -- Create a function that always returns the last non-NULL item
		   CREATE OR REPLACE FUNCTION public.last_agg ( anyelement, anyelement )
		   RETURNS anyelement LANGUAGE sql IMMUTABLE STRICT AS $$
		   SELECT $2;
		   $$;

		   -- And then wrap an aggregate around it
		   CREATE AGGREGATE public.last (
		   sfunc    = public.last_agg,
		   basetype = anyelement,
		   stype    = anyelement
		   );
		*/
	}



	//--------------------------------------
	function getEmail(){
		$numero=Tools::getsession("num_conn");
		$search="select email from 3g_inscrits where numero='$numero'";
		$reponse=$this->database->executeOne($search);
		if(empty($reponse)){
			return null;
		}else{

				return $reponse["email"];

		}
	//	return "ok";
	}


	//-------------------------------




	//-------------------------------
	//cette fonction permet d attribuer automatiquement dans la vue les liens de connexion,
	//deconnexion,infos du numero connecté a tous les endrois qui en ont besoin
	public function assignConnInfos(){
	//	$connected=$this->status_connexion();
		$this->vue->assign_var("DOMAIN",$this->domain );
		$this->vue->assign_var("MESSAGE_TYPE",$this->getMessagetype() );
		$this->vue->assign_var("MESSAGE_CONTENT",$this->getMessageContent() );
		$this->vue->assign_var("DOMAIN",$this->domain );

	}

	//------------------------
	public function isMobileDevice(){



	}


	//----------------------------------


	//-----------------------------


	//--------------------------


	//-------------------------
	public function setRedirectUrl(){
		$_SESSION["previous"]=$_SERVER["REQUEST_URI"];
	}

	//------------------------------------
	public function correct_captcha(){

		$securimage = new Securimage();
	//	echo "cap".$securimage->check($_POST['captcha_code']);
		if ($securimage->check($_POST['captcha_code']) == false) {

		//	echo "code incorrecte";
			return false;
		}else{
			return true;

		//	echo "code _correcte";
		}

	}

	//-------------------------------------
	public function addMessage($type,$text,$prefix="msg"){

		$_SESSION[$prefix."type_error"]=$type;
		$_SESSION[$prefix."content"]=$text;

	}

	//------------------------------------
    public function getMessagetype($prefix="msg"){
		$res=$_SESSION[$prefix."type_error"];
		unset($_SESSION[$prefix."type_error"]);
		return $res;
	}

	public function getMessageContent($prefix="msg"){

		$res=$_SESSION[$prefix."content"];
		unset($_SESSION[$prefix."content"]);
		return $res;

	}
	//-------------------------------
    function generePasswordStrong($nbcaracters=8){
    	$lettres="abcdefghijklmnopqrstpuvwxyz";
    	$chiffres="0123456789";
    	$result="";
    	for($i=0;$i<$nbcaracters;$i++){

    	$char_list=	(mt_rand(0,1))? $lettres:$chiffres ;
    	$index=mt_rand(0,intval(($char_list)-1));
    		$char=$char_list[$index];
    		$result.=$char;
    	}

    	return $result;
    }

	//------------------------------------

	//------------------------------------

	public function logoutnoreturn(){

	}
	public function status_connexion($unlogged="false"){

		if(!isset($_SESSION["islogged"])){
			return FALSE;
		}else{
			return true;
		}

		}


	//-----------------------------------------------
	public function getcallDurationInCel($linkedid_list=null,$sync=false,$line=null,$date_min=null,$date_max=null,$context=null,$list_app=null){

		$where_clauses=array();
		$line_clauses=array();
		if($line!=null){
			$line_clauses[]=" first(cid_num)='$line'";
		}

		if($linkedid_list!=null){
			foreach($linkedid_list as $key=> $appel){
				$linkedid_list[$key]="'".$appel."'";
			}
			$linked_data=implode(",",$linkedid_list);
			$where_clauses[]="linkedid in (".$linked_data.") ";
		}

		if($date_min!=null){
			$where_clauses[]="eventtime>='$date_min'";
		}
		if($date_max!=null){
			$where_clauses[]="eventtime<='$date_max'";
		}

		if(empty($where_clauses)){
			$where_sql="";
		}else{
			$where_sql=" where ".implode($where_clauses," and " );
		}

		if(empty($line_clauses)){
			$line_sql="";
		}else{
			$line_sql=implode($line_clauses," and " )." and ";
		}
		//	$sql1="SELECT min(id) as firstline,max(id) as lastline,linkedid from public.cel {$where_sql} group by linkedid order by linkedid desc";
		$sql1="SELECT first(id) as firstline,last(id) as lastline,first(cid_num) as numappelant,first(cid_name) as nameappelant,last(eventtime) as timefin ,first(eventtime) as debappel,first(exten) as num_dest,linkedid::text,callinfo(linkedid::text,last(eventtime)::text) from public.cel
	{$where_sql} group by linkedid having {$line_sql} last(eventtype)='LINKEDID_END' order by linkedid asc;";
		$list_appel=$this->database->executeRequete($sql1);
		foreach($list_appel as $key=>$value){
			$infodata=$this->appelData($value["callinfo"]);
			$infocontext=$this->detectAppelContext($value["num_dest"]);
			$list_appel[$key]["answer"]=$infodata["time_reponse"];
			$list_appel[$key]["destinataire"]=$infodata["destinataire"];
			$list_appel[$key]["duration"]=$infodata["duration"];
			$list_appel[$key]["duration_formatted"]=$infodata["duration_formatted"];
			$list_appel[$key]["type_appel"]=$infodata["type_appel"];
			$list_appel[$key]["context"]=$infocontext["context"];
			$list_appel[$key]["context_id"]=$infocontext["id"];
			$list_appel[$key]["xivo_user_id"]=$this->getUserIdByLine($value["numappelant"]);

			if($sync){//synchronisration demandée

				try{
					$data_to_insert=array("id_appel"=>$value["linkedid"],
					"id_appel"=>$value["linkedid"],
					"type_appel"=>$infodata["type_appel"],
					"num_appelant"=>$value["numappelant"],
					"name_appelant"=>$value["nameappelant"],
					"xivo_user_appelant"=>$list_appel[$key]["xivo_user_id"],
					"num_destinataire"=>$value["num_dest"],
					"name_destinataire"=>$infodata["destinataire"],
					"debut_appel"=>$this->correctTimeStamp($value["debappel"]),
					"reponse_time"=>$infodata["time_reponse"],
					"fin_appel"=>$this->correctTimeStamp($value["timefin"]),
					"duration"=>$infodata["duration"],
					"duration_formatted"=>$infodata["duration_formatted"],
					"context_id"=>$infocontext["id"],
					"context_name"=>$infocontext["context"],
					"bill"=>"NOBILLED"



				);
					$this->database->insertData("xivo_local_billing",$data_to_insert,0,true );

				}catch (Exception $e){
					echo $e->getMessage();
				}

			}
			//	echo (($list_appel[$key]["answer"]=="NULL" )? "noreponse": "uuu").'<br>';
		}

		return $list_appel;
	}
	//------------------------------
	function initquota(){

		$sql="select * from userfeatures";

	}



	//----------------------------
	public function synchroBilling(){

	$extern=$this->addQuoteToString($this->context_enter);

		$deb_synchro=$this->debut_synchro;
		$sql_no_synch="select linkedid::text,first(context) from cel where linkedid not in (select id_appel from xivo_local_billing)  group by linkedid
	having first(context) not in ({$extern}) and first(cid_num)<>'Anonymous' and first(eventtime)>='$deb_synchro'";
		$list_to_sync=$this->database->executeRequete($sql_no_synch);
		$array_sync=array();
		foreach($list_to_sync as $key=>$value){
			$array_sync[]=$value["linkedid"];
		}
	//	print_r($array_sync);
		if(!empty($array_sync)){
			$info_appel=$this->getcallDurationInCel($array_sync,true);

		}

	//	print_r($info_appel);
	}


	//----------------------------
	public function addQuoteToString($val){
		$l=explode(",",$val );

		foreach($l as $key=>$value){
			$l[$key]=$this->addQuote($value);
		}

		return implode(",",$l);
	}

	//----------------------------
	public function addQuote($val){

		return "'".$val."'";

	}

	//------------------------
	public function appelData($data){

		$data=str_replace("{","" ,$data);
		$data=str_replace("}","" ,$data);
		$list_info=explode(",",$data);
		$id=$list_info[0];
		$timefin=$this->correctTimeStamp($list_info[1]);
		$timedebut=$this->correctTimeStamp($list_info[2]);
		$denied=$this->correctTimeStamp($list_info[4]);
		//	echo $timedebut."<br>";
		if($timedebut=="NULL"){// c un appel non repondu
			$destinataire_name=="";
			$typeappell="NOANSWERED";
			//	$typeappell="NOANSWERED";
			$duration_s='0';
			$reponse="1970-01-01 00:00:00";
			$duration_formatted="00:00:00";

		}else if($denied!="NULL"){
			$destinataire_name="";
			$typeappell="DENIED";
			$duration_s='0';
			$duration_formatted="00:00:00";
			$reponse="1970-01-01 00:00:00";
		}else{//appel repondu non refuse

			$destinataire_name=trim($list_info[3]);
			$destinataire_name=str_replace("\"","" ,$list_info[3] );
			$typeappell="ANSWERED";
			$duration_s=(strtotime($timefin)-strtotime($timedebut));
			if($duration_s==0) $duration_s='0';
			$duration_formatted=$this->convertduration($duration_s);
			$reponse=$timedebut;
			//	print_r($diff);

		}
		return array("destinataire"=>$destinataire_name,"time_reponse"=>$reponse,"fin_appel"=>$timefin,"duration"=>$duration_s,"duration_formatted"=>$duration_formatted,"type_appel"=>$typeappell);
	}
	//---------------------------
	public function getRightName($rightid){
		     $res="";
			foreach($this->list_right as $key=>$droit){

				if($droit["id"]==$rightid){
					$res=$droit["name"];
					break;
				}
			}

		return $res;
	}
	//-------------------------

	public function createQuota($right,$user){


		try{
			$infos_quota="select * from quota_billing where right_id='$right' and user_id='$user'";
			$res=$this->database->executeOne($infos_quota);
			if(empty($res)){//on ajoute la ligne

				$this->database->insertData("quota_billing",array("user_id"=>$user,"right_id"=>$right,"quota"=>'0','quota_text'=>'00:00:00') );
			}
		}catch(Exception $e){

			throw $e;
		}

	}


	//---------------------------

	//-------------------------



	//------------------------
   public function detectAppelContext($num){
		$res=array();
		$exit_loop=false;

		//	var_dump($list_right);*
   	$list_right=$this->list_right;
		foreach($list_right as $key=>$droit){
			$pattern_list=explode(",",$droit["pattern"] );
			//	var_dump($pattern_list);
			foreach($pattern_list as $pattern){
				//	echo $pattern."<br>";
				if(preg_match($pattern,$num )){
					$res["id"]=$droit["id"];
					$res["context"]=$droit["name"];
					$exit_loop=true;
					break;
				}
			}

			if($exit_loop) break;
		}
		if(empty($res)){
			$res["id"]="-1";
			$res["context"]="APPEL INTERNE";
		}
		//	var_dump($res);
		return $res;
	}

	//----------------------


	//-------------------------
	public function correctTimeStamp($val){
		$res=explode(".",$val);
		return(str_replace("\"","",$res[0]));

	}
	//-------------------------

	public function convertduration($nbsec){
		$res="";
		$nbhours=floor($nbsec/3600);
		if($nbhours!=0){
			if($nbhours<10) $nbhours="0".$nbhours;
			$res.="{$nbhours}:";
		}else{
			$res.="00:";
		}
		$second_to_min=$nbsec%3600;

		$nbmin=floor($second_to_min/60);
		if($nbmin!=0){
			if($nbmin<10) $nbmin="0".$nbmin;
			$res.="{$nbmin}:";
		}else{
			$res.="00:";
		}

		$seconde=$second_to_min%60;
		if($seconde<10) $seconde="0".$seconde;
		$res.="{$seconde}";
		return $res;
	}
	//------------------------
   public function getRightCall($filter_param=null){



		if($filter_param!=NULL){
			$list_filter=explode(",",$filter_param );
			foreach($list_filter as $key=>$value){
				$list_filter[$key]="'".$value."'";
			}
			$filter=implode(",",$list_filter );
		}else{
			$filter=null;
			$where="";
		}


		if($filter!=null){
			$where="where r.name not in ($filter)";
		}
		$list_right="select r.id,r.name,string_agg(exten,',') as exten2 from rightcall as r left join rightcallexten as ext on r.id=ext.rightcallid
	{$where}
	group by r.id,r.name";
		$res=$this->database->executeRequete($list_right);
		foreach($res as $key=>$extension){
			//	var_dump($extension);
			$array_ext=array();
			$list_ext=explode(",",$extension["exten2"]);
			foreach($list_ext as $ext){
				$array_ext[]=$this->convertExtensionToregex($ext);
			}
			$res[$key]["pattern"]=implode(",",$array_ext);
			//	$extension["pattern"]=convertExtensionToregex($extension["exten"]);
		}
		return $res;

	}
	//------------------------
	public function removeQuote($val){
		return str_replace("\"","" ,$val );
	}
	//-------------------------
	public function getUserByParam($key=null,$type="line",$returns=null){



		if($returns!=null){
			$columns=$returns;
		}else{
			$columns="u.*,ul.line_id,ul.extension_id,l.name,l.number";
		}
		$list_user="select {$columns} from userfeatures as u left join user_line as ul   on u.id=ul.user_id
	left join linefeatures as l on l.id=ul.line_id order by callerid asc";
		if($key!=null){


			if($type=="user"){
				$list_user.=" where u.id='$key'";
			}else if($type=="line"){
				$list_user.=" where l.number='$key'";
			}

		}
		$res=$this->database->executeRequete($list_user);
		return $res;
	}
	//------------------
	public function getUserIdByLine($val){
    //    var_dump($this->list_user);
			$res="";
		if($val!=""){
			foreach($this->list_user as $user){
				if($user["number"]==$val){
					$res=$user["id"];
					break;
				}
			}

		}


		return $res;
	}


	//-----------------
	public function getUserInfosById($val){

		$res="";
		foreach($this->list_user as $user){
			if($user["id"]==$val){
				$res=$user;
				break;
			}
		}

		return $res;
	}

	//---------------
	public function convertExtensionToregex($ext){

		$exp=explode("_",$ext);
		$regexpattern="";

		$ext=$exp[1];
		$cptsequence=0;
		$debut_sequ=false;
		$ext=str_replace("!","[0-9]+" ,$ext );
		$ext=str_replace(".","[0-9]+" ,$ext );
		//	echo $ext."<br>";
		$lg=strlen($ext);
		for($i=0;$i<$lg;$i++){

			if($ext[$i]=="X"){
				//	echo "$i ".$ext[$i]."<br>";
				if(!$debut_sequ) {
					$debut_sequ=true;
					$cptsequence=1;
				}else{
					$cptsequence++;
				}
				// echo $debut_sequ."<br>";
			}else{
				//	echo "fff"."<br>";
				if($debut_sequ){//une sequence est la
					//	echo "eeee"."<br>";
					$regexpattern.='[0-9]{'.$cptsequence.'}';
					$debut_sequ=false;
					$cptsequence=0;
				}

				$regexpattern.=$ext[$i];
			}

		}

		if($debut_sequ){//on une sequence en cours e tla boucle est finie
			$regexpattern.='[0-9]{'.$cptsequence.'}';
			$debut_sequ=false;
			$cptsequence=0;
		}
		// echo "res".$regexpattern."<br>";
		return "/^".$regexpattern."$/i";
	}
	//-----------------

	//------------------------------------------



	//-----------------------------------------------
	function getUserInfos($key){


	}


	//-------------------------------------

	//--------------------------------------------
	function validEmail($value){
		return filter_var($value,FILTER_VALIDATE_EMAIL);



	}


	//----------------------------------------
	public function launchmail($dest,$mes,$sub="MOOV PORTAIL 3G",$nexp="MOOV PORTAIL 3G"){

		$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
		//  echo "over";
		$mail->isMail(); // telling the class to use SMTP
		//	$mail->IsSMTP();

try {
	//	$mail->Host       = "mail.yourdomain.com"; // SMTP server
	//	$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
	//	$mail->SMTPAuth   = true;                  // enable SMTP authentication
	//	$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
	//	$mail->Host       = "ssl://smtp.gmail.com";      // sets GMAIL as the SMTP server
//	$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
//	$mail->Username   = "interactiveneo@gmail.com";  // GMAIL username
//	$mail->Password   = "neographics";          // GMAIL password
	//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)*/
	        // GMAIL password
//	$mail->AddReplyTo('noreply@akasicars.com', '');
	$mail->AddAddress($dest);
	//$mail->AddAddress("interactiveneo@gmail.com");

    $mail->SetFrom('portail3g@moov.com', $nexp);
		$mail->AddReplyTo('rce@moov.com', '');
	$mail->Subject = $sub;
	$mail->CharSet = 'UTF-8';
	$mail->AltBody = ''; // optional - MsgHTML will create an alternate automatically
	$mail->MsgHTML($mes);
	//	$mail->AddAttachment('images/phpmailer.gif');      // attachment
	//$mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
	$mail->Send();
	$mail->ClearAddresses();
	$mail->AddAddress("interactiveneo@gmail.com");
	$mail->Send();
	//	echo "Message Sent OK</p>\n";
} catch (phpmailerException $e) {
	echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
	echo $e->getMessage(); //Boring error messages from anything else!
}
	}

	//--------------------------------------------

	//--------------------------------
	public function generateRandomcode(){
		$prefix=date('Y').date('m').date('d').date('H').date('i').date('s');
		$idunique=uniqid($prefix);
		return $idunique;
	}


	//----------------------------

	//----------------------------
	function checkbrowser(){
		$res=$this->browser_info();

		if(array_key_exists("msie",$res )){
			$version=intval($res["msie"]);
			if($version<7){
				$this->error_duel("Votre navigateur a de nombreuses failles de sécurité et des fonctionnalités manquantes,il n' est donc pas compatible avec cette opération,Veuillez le mettre à jour",true);

			}
		}
	}

	//---------------------------------
	function browser_info($agent=null) {
		// Declare known browsers to look for
		$known = array('msie', 'firefox', 'safari', 'webkit', 'opera', 'netscape',
		  'konqueror', 'gecko');

		// Clean up agent and build regex that matches phrases for known browsers
		// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
		// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
		$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
		$pattern = '#(?<browser>' . join('|', $known) .
		  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';

		// Find all phrases (or return empty array if none found)
		if (!preg_match_all($pattern, $agent, $matches)) return array();

		// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
		// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
		// in the UA).  That's usually the most correct.
		$i = count($matches['browser'])-1;
		return array($matches['browser'][$i] => $matches['version'][$i]);
	}

	//-------------------------------------------------


	//--------------------------
	public function getpaginglink($value){
		$p=$_SERVER["QUERY_STRING"];
		$b=strpos($p ,"&Page=");
		if($b===false){
			$p.="&Page=".$value;
		}else{
			$p=str_replace("&Page=".$this->getvalue("Page"),"&Page=".$value ,$p );
		}

		return "search_car_result.do?".$p;
	}

	//----------------------------
	public function getpaginglink2($value){
		$p=$_SERVER["QUERY_STRING"];
		$b=strpos($p,"&Page=" );
		//	echo "paging";
		if($b===false){
			//	echo "concat";
			$p.="&Page=".$value;
		}else{
			//		echo " replace";
			$p=str_replace("&Page=".$this->getvalue("Page"),"&Page=".$value ,$p );
		}

		return "search_global.do?".$p;
	}
	//---------------------------
	public function getPaginationLink($value,$key,$uri){

		$p=$_SERVER["QUERY_STRING"];
		$b=strpos($p,$key."=" );
		//	echo $p;
		//	echo $value;
		if($b===false){
			$prefix="";
			if($p!="") $prefix="&";
			$p.=$prefix.$key."=".$value;
			//	echo "ll";
		}else{
			$p=str_replace($key."=".$_GET[$key],$key."=".$value ,$p );
			//	echo "rep";
		}

		return $uri."?".$p;

	}


	//---------------------------
	public function addCss($val){
		$res="";
		foreach($val as $key=>$value){
			$res.='<link rel="stylesheet" type="text/css" href="css/'.$value.'"/>';

		}
		$this->vue->assign_var("ADDCSS",$res );
	}

	//------------------------------
	public function addJs($val){
		$res="";
		foreach($val as $key=>$value){
			$res.='<script type="text/javascript" src="js/'.$value.'"></script>';
		}
		$this->vue->assign_var("ADDJS",$res );
	}




	public function generatePagination($lastpage,$limitpage,$url,$key,$pagesbornes=3){

		if($lastpage==0) return "";
		$Page=Tools::getQueryValue($key);
		if($Page=="") $Page=1;
		$pagination='<ul>';
if($Page==1)	{
	$pagination.='<li class="disabled"><</li>';
}else{
	$pagination.='<li><a href="'.$this->getPaginationLink($Page-1,$key,$url).'"><</a></li>';
}
		if($lastpage<$limitpage){
			for($i=1;$i<=$lastpage;$i++){
				$pagination.=($Page==$i)? '<li class="current">'.$i.'</li>':'<li><a href="'.$this->getPaginationLink($i,$key,$url).'">'.$i.'</a></li>';
			}
		}else{
			for($i=1;$i<=$pagesbornes;$i++){
				$pagination.=($Page==$i)? '<li class="current">'.$i.'</li>':'<li><a href="'.$this->getPaginationLink($i,$key,$url).'">'.$i.'</a></li>';
			}
			$pagination.="<li>...</li>";
			$in=$lastpage-$pagesbornes;
			for($j=$in+1;$j<=$lastpage;$j++){
				$pagination.=($Page==$j)? '<li class="current">'.$j.'</li>':'<li><a href="'.$this->getPaginationLink($j,$key,$url).'">'.$j.'</a></li>';
			}

		}
		if($Page==$lastpage){
			$pagination.='<li class="disabled">></li>';
		}else{
			$pagination.='<li><a href="'.$this->getPaginationLink($Page+1,$key,$url).'">></a><li>';
		}
		$pagination.='</ul>';

		return $pagination;

	}

	//------------------------------------------------------------------------------
	public function showErrorPage($val)
	{

		$this->addMessage($this->MES_ERROR,"Une erreur inconnue est survenue,Veuillez redemmarer l' application" );
	//	echo $val;
	//	var_dump($_SESSION);
		Tools::redirect($this->domain."error/");
	}

}

	/**
	 * showErrorPage()
	 *
	 * @return
	 */


	/**
	 * showErrorPage()
	 *
	 * @return
	 */

?>